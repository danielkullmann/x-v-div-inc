"use strict;"

/*
row and col are zero-based, i.e. 0-8
player is either 0 or 1
*/
const ROWS = 9, COLS = 9;

const GAME_RULES = {
    // TODO create rules for the game, which can be added to the game:
    // 1. Suspend box rule of Sudoku (every 3x3 box may only contain each number once)
    // 2. Suspend rule that the same rule may not be reused in the next move
    // 3. Suspend rule that the next number may not be the same as the previous number
    //    (i.e. 5-6-5 is now allowed)
    // 4. Add restriction that all neighboring cells must respect the rules
    // 5. Suspend or add rule that the crossing of a snake is restricted to use unused rules  
    // 6. Suspend rule that the next number may not use more than one rule 
};

const STEP_NAMES = [
    "X",
    "V",
    "./.",
    "+-1"
];

const STEPS = {
    "X": function(number) {
        return [10-number];
    },
    "V": function(number) {
        if (number >= 5) {
            return [];
        }
        return [5-number];
    },
    "./.": function(number) {
        let half = parseInt(number / 2);
        let double = number * 2;
        let result = [];
        if (2*half === number) {
            result.push(half);
        }
        if (double <= 9) {
            result.push(double);
        }
        return result;
    },
    "+-1": function(number) {
        let result = [];
        if (number > 1) {
            result.push(number - 1);
        }
        if (number < 9) {
            result.push(number + 1);
        }
        return result;
    }
};

const Game = function() {
    this.state = [];
    for (let r = 0; r < ROWS; r++) {
        let row = [];
        for (let c = 0; c < COLS; c++) {
            row.push(0);
        }
        this.state.push(row);
    }
    this.players = [{row: 0, col: 0}, {row: 0, col: 0}];
    this.calculateGameSteps();
}

Game.prototype.start = function(player, row, col) {
    this.assertPosition(row, col);
    var otherPlayer = 1 - player;
    if (this.players[otherPlayer].row !== 0) {
        if (this.players[otherPlayer].row === row && this.players[otherPlayer].col === col) {
            throw new Error("player start position is already taken: " + 
                player + " " + row + " " + col);
        }
    }
    this.players[player] = {
        row: row,
        col: col
    }
}

Game.prototype.checkMove = function(row, col, number) {
    if (!this.checkIsNeighbor(row, col)) {
        throw new Error("illegal neighbor",row,  col);
    }
    let currentNumber = this.state[this.row][this.col];
    if (currentNumber === 0) {
        throw new Error("illegal state; current cell is zero", row, col);
    }
}

Game.prototype.checkIsNeighbor = function(player, row, col) {
    // TODO
    throw new Error("Not implemented");
}

Game.prototype.getNeighbors = function(playerPos) {
    let row = playerPos.row;
    let col = playerPos.col;
    let result = [];
    if (row-1 >= 0) {
        result.push([row - 1, col]);
    } 
    if (row+1 < ROWS) {
        result.push([row + 1, col]);
    } 
    if (col-1 >= 0) {
        result.push([row, col - 1]);
    } 
    if (col+1 < COLS) {
        result.push([row, col + 1]);
    } 
    return result;    
}

Game.prototype.assertPosition = function(row, col) {
    if (row < 0 || row >= ROWS) {
        throw new Error("illegal row", row);
    }
    if (col < 0 || col >= COLS) {
        throw new Error("illegal col", col);
    }
}

Game.prototype.calculateGameSteps = function(row, col) {
    let GameSteps = {};
    let GameStepsNoDuplicates = {};
    for (let number = 1; number <= 9; number++) {
        GameSteps[number] = {};
        GameStepsNoDuplicates[number] = {};
        let numberCounts = {};

        for (let stepName of STEP_NAMES) {
            let nextNumbers = STEPS[stepName](number);
            GameSteps[number][stepName] = nextNumbers;
            GameStepsNoDuplicates[number][stepName] = nextNumbers;
            for (let nextNumber of nextNumbers) {
                numberCounts[nextNumber] = (numberCounts[nextNumber] || 0) + 1;            
            }
        }
    
        for (let key in numberCounts) {
            // NOTE: key is a string (all Object keys are strings)
            if (numberCounts[key] > 1) {
                for (let stepName of STEP_NAMES) {
                    let nextNumbers = GameStepsNoDuplicates[number][stepName];
                    nextNumbers = nextNumbers.filter((nextNumber) => nextNumber != key);
                    GameStepsNoDuplicates[number][stepName] = nextNumbers;
                }
            }
        }
    }
    
    this.GameSteps = GameSteps;
    this.GameStepsNoDuplicates = GameStepsNoDuplicates;
};

let game = new Game();
game.start(0, 2, 4);
game.start(1, 3, 6);
console.log(JSON.stringify(game.getNeighbors(game.players[0])));
